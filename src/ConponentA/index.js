import React from "react";
import styled from "styled-components";

import Icon from "./Icon";
import Body from "./Body";

const Container = styled.div`
  height: 80px;
  width: 350px;
  display: flex;
  flex-direction: row;
  margin-top: 15px;
`

function App(params) {
  return (
    <Container>
      <Icon ico={params.icon} colorIcom={params.colorIcom}/>
      <Body />
    </Container>
  );
}

export default App;
