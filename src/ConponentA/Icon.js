import React from "react";
import styled from "styled-components";


const Container = styled.div`
  width: 25%;
`

const Icon =styled.img`
  height: 20px;
`

const Body = styled.div`
  background-color: ${props => props.colorIcom};
  height: 100%;
  width: 100%;
  border-radius: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
`

function App({ico, colorIcom}) {
  return (
    <Container>
        <Body colorIcom={colorIcom}>

        <Icon  src={ico}/>

        </Body>
      
    </Container>
  );
}

export default App;
