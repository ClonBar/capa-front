import React from "react";
import styled from "styled-components";


const Container = styled.div`
  width: 75%;
  padding-left: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`

const Title = styled.p`
  margin: 0%;
  color: #858ea3;
  font-weight: bold;
  font-size: 15px;
`

const Description = styled.p`
  margin: 0%;
  margin-top: 5px;
  color: #bcc1c5;
  font-size: 13px;
`

const Link = styled.a`
  margin: 0%;
  margin-top: 5px;
  color: #3b8695;
  font-size: 13px;
  font-weight: bold;
`

function App() {
  return (
    <Container>

      <Title>Paquete Premium</Title>

      <Description>Descubre nuevas funciones</Description>

      <Link>Hazte premium</Link>

    </Container>
  );
}

export default App;
