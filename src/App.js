import React from "react";
import styled from "styled-components";

import ComponentA from "./ConponentA"

import corona from "./images/Corona.png";
import domain from "./images/Domain.png";
import email from "./images/Email.png";

const Container = styled.div`

`

function App() {
  return (
    <Container>
      <ComponentA icon={corona} colorIcom={"#fff6e5"}/>
      <ComponentA icon={domain} colorIcom={"#ebfcf1"}/>
      <ComponentA icon={email} colorIcom={"#f4f2ff"}/>
    </Container>
  );
}

export default App;
